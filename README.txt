Version 1.0 2021-11-01
                              ____..... .-----....__ _                          
                       __.--~~         ;          _.i'~~--.__                   
                   _.-'        '     ,'            `.        `-._               
                .-'  '           _.·:    '            '          `-.            
              .'\ _.    _____)       `                         '  _/`.          
             /   ;    /                   /)            ,        ' \  \         
            i         )__   _ _   _  __  // _   _  _/_   __   _     `-.i        
            !  '    /       (/___(/_/ (_(/_(_(_/_)_(___(_/ (_(_/_      !        
           i   '   (_____)                                  .-/   '  .\ i       
          .-.._           ____  ___)           '           (_/     .'| \!       
       _ |  .' `'-.      (, /   /                /)      /)  /)   / \'/ |       
      / `._.-,._|_ \       /---/  ___    _    _ (/   ___// _(/    |  |/ '.-'\   
     |  / |.-/  \ `,    ) /   (__(_)(_(_/_)__(/_/ )_(_)(/_(_(_   |\ \| // | /   
.--- `._\_`-',._/  |   (_/      __                              .' `._'.-.'  `. 
|`./ |   ;`._/ \   /        ___/__)                           '  | ,' `.  | _|  
; _\.|;   \    |.-' '      (, /   ____ _   _                    ; _|_.,-._.' \; 
 \   '.`-'``._./             /   (_) (/___(/_    Adel Faure     ,' /  \-.| \  | 
  `'--'  |   _.-     '      (______                             |  \_.,`-'_/_.' 
    .''-. >-~-.    '    '          )        '                '  \   / \_.';'  | 

         To play type commands numbers or letters between brackets then 
                          press [ ENTER ] to validate.


-> If the new occupants spot you they will call an exorcist    
-> The new occupants will eventually dirty the rooms they walks through
-> The new occupants always keep their daily routine        
-> At day 4 everything must be clean before midnight           


                                   Manor map
                                   ---------

1st_floor______________       _____      2nd_floor______________                
|                   |==|    .'     `.    |                   |==|               
| [ 0 ] Living room |==|   /    |    \   | [ 5 ] North bedroom==|               
|                   |==|  i     |     i  |                   |==|               
|                   |==|  !           !  |                   |==|               
|                      |   \         /   |    A             _|  <- [ 4 ] Hallway
|                      |    `._____.'    |                  _   |               
|_______| |____________|______________   |___________________|  |______________ 
|                      |              |  |                   |  |              |
| [ 1 ] Dining room    | [ 2 ] Kitchen|  | [ 6 ] South bedroom  | [ 7 ] Bathroom
|                      |              |  |                   |  |              |
|                     _|              |  |                  _|  |_             |
|                     _               |  |      B           _    _             |
|                      |              |  |                   |  |              |
|                      |              |  |                   |  |              |
|_______| |____________|______________|  |___________________|__|______________|
:                                     :                                         
: [ 3 ] Yard                          :  @ = ghost (you)  A B = occupants       
:           @                         :  E = exorcist                           
:.....................................:                                         

[ 0-8 ] Move   
[ L ]   Look   
[ W ]   Wait

                                  Room example
                                  ------------

 _._ .-._ _._                    |=|=|=|=|=|,8.|=|=|             _._ .-._ _._   
: .-:_:_.':::`.              _.-'-~-~-~-~ ,8°_°8.-~-`---------.-: .-:_:_.':::`-.
.:-.'_.-:.-.:-.)         _.-'- - - - - -,8°i[=]i°8.- - - - - -|:.:-.'_.-:.-.:-::
' .:  `.-:-.): ))    _.-' - - - - - - ,8°:¯-¯-¯-¯:°8. - - - - |:' .:  `.-:-.):: 
: -.:  .::`.:.: )_.-'- - - - - - - -,8T°`TT=====TT'°T8.- - - -|:: -.:  .::`.:.::
::: \:/ : ::::.-'_- - - - - - - - - - |--||     ||--| - - - - | ::: \:/ : :::: :
  -.ii-'  :`-.:¯`.|==|==========L==L==|· ||_____||· |=L===L===!:  -.i-..  :`-.: 
 :. !!:  ::.-'::))|· |· · · · · |  |· | ·||     || ·|·|   | |::: :.:  :\\::.-' :
.-:=.ii:.==.-~.;))| ·| · . · · ·!  ! ·|· ||_____||· | |   |·|:: .-:=.ii:.==.-~:/
 /  \\ //; `._`.) |· |·  |=|=|=|=|=|=|L ·TT-----TT ·|.|...|.|:::.-' \\ //; `._:)
:`.-.i i-. ':.-'  | ·|,8°-¯-¯-¯-¯-¯-¯-°8.°¯-¯-¯-¯°8.||=|=|=|=| ::`.-.iii-. ':.' 
-'-' ! !-'`-'     |· |~~T[][][]I[][][]T~`TT=====TT~ |- - - - -°8.-'-'| |-'`-'   
     / /        .8|--|--||/¯|¯¯¯¯¯|¯\||--||     ||--|[][]I[][]T~~    | |        
|-|-| |-|-|-|-|'°L|· |· ||· |==T==|· ||· ||_____||· |/|____|\||-|-|-|-\ \|-|-|-|
·   ! !  ·  ·   ·|| ·| ·|| ·||||||| ·|| ·||     || ·|·|||||| ||  ·    | | ·  ·  
  ·  \ \ · ·  ·  ||· |· ||· |==|==|· ||· ||_____||· | ||==|| ̣||    ·  | | · ·   
 :   ! !    :    L|__|__||__|__|__|__||__TT-----TT__|_||__||_|| · :   ! !    :  
:  :/   `-._ :   ||L=|-.=.===========.=.-iL=L=L=LiL=|________||  :  :/   \ :   :
 :   :     :   : ^^--^-|L|===========|L|-|=L=L=L=|--^---------^   :   :     :   
: :  ::  :  ::  :: ::   ,'           `. : ¯¯¯¯¯¯¯: :: ::  :  ::  :  : ::  ::  : 
You are in the Yard.   

[ C ] Clean
[ M ] Manor map

---

Everlasting Household Love, copyright (C) 2021 Adel Faure,
contact@adelfaure.net, https://gitlab.com/adelfaure/everlasting-household-love

This program is free software: you can redistribute it and/or modify it under 
the terms of the GNU General Public License as published by the Free Software 
Foundation, either version 3 of the License, or (at your option) any later 
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY 
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with 
this program. If not, see https://www.gnu.org/licenses/.

This game source is based on ASCII Facemaker, copyright (C) 2021 Adel Faure,
contact@adelfaure.net, https://gitlab.com/adelfaure/ascii-facemaker

I'd like to thank Mistfunk, Textmode Friends, SoloDevlopment members and people
who follow me on twitter for their warm support and encouragement.

Fonts License: jgs5 is under SIL Open Font License 1.1
