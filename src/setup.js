/*
    Everlasting Household love, copyright (C) 2021 Adel Faure,
    contact@adelfaure.net

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

var time, day, characters, positions, rooms, filth, a_schedule, b_schedule, e_schedule, e_counter = 0, exorcism;

function init(){
  time = 23; 
  day = -1;
  characters = ['@','A','B','E'];
  positions = [3,5,6,null];
  rooms = ['Living room','Dining room','Kitchen','Yard','Hallway','North bedroom','South bedroom','Bathroom'];
  filth = [7,7,7,7,7,7,7,7];
  a_schedule = [
    [5, 4],
    [6, 7],
    [7, 4],
    [8, 0],
    [9, 1],
    [10, 2],
    [11, 1],
    [13, 3],
    [14, null],
    [17, 3],
    [18, 1],
    [19, 2],
    [20, 1],
    [21, 0],
    [22, 4]
  ]
  b_schedule = [
    [6, 4],
    [7, 7],
    [8, 4],
    [9, 0],
    [11, 1],
    [14, 0],
    [16, 1],
    [17, 3],
    [19, 1],
    [21, 0],
    [22, 4],
  ]
  e_schedule = [
    3,
    1,
    2,
    1,
    0,
    4,
    7,
    4,
    6,
    4,
    5,
    4,
    0,
    1,
    3,
  ]
  e_counter = 0;
  exorcism = false;
  if (audio_triggered) {
    audio.pause();
    audio.src = 'src/souvenirs_de_chez_soi_low.ogg';
    audio.currentTime = 0;
    audio.play();
  }
  wait();
}

function wait(){
  day = time > 22? day + 1: day;
  time = time < 23? time + 1: 0;
  mode = 0;
  if (time > 5) mode = 1;
  if (time > 11) mode = 2;
  if (time > 17) mode = 3;
  if (time > 22) mode = 0;
  positions[1] = 5;
  for (var i = 0; i < a_schedule.length; i++){
    if (time > a_schedule[i][0]) {
      positions[1] = a_schedule[i][1];
    }
  }
  positions[2] = 6;
  for (var i = 0; i < b_schedule.length; i++){
    if (time > b_schedule[i][0]) {
      positions[2] = b_schedule[i][1];
    }
  }
  if (mode != 0 && mode != 3) {
    if (filth[positions[1]] < 7) {
      if(Math.random()>0.8)filth[positions[1]]++;
    }
    if (filth[positions[2]] < 7) {
      if(Math.random()>0.8)filth[positions[2]]++;
    }
    if (filth[positions[3]] < 7) {
      if(Math.random()>0.8)filth[positions[3]]++;
    }
  }
  if (exorcism) {
    if (!e_counter) {
      audio.pause();
      audio.volume = 0.2;
      audio.src = 'src/exorcism_low.ogg';
      audio.currentTime = 0;
      audio.play();
    }
    mode = 4;
    positions[3] = e_schedule[e_counter]
    e_counter++;
    if (e_counter > e_schedule.length) {
      e_counter = 0;
      audio.pause();
      audio.src = 'src/souvenirs_de_chez_soi_low.ogg';
      audio.currentTime = 0;
      audio.play();
      exorcism = false;
      mode = 0;
      if (time > 5) mode = 1;
      if (time > 11) mode = 2;
      if (time > 17) mode = 3;
      if (time > 22) mode = 0;
      positions[3] = null;
    }
  }
}

function is_spotted(){
  for (var i = (exorcism? 3: 1); i < positions.length; i++){
    if (positions[i] == positions[0]) {
      encounter_menu(characters[i]);
      return true;
    }
  }
  return false;
}

function print_map(){
  var map = assets['map'];
  for (var i = 0; i < filth.length; i++) {
    if (filth[i]) continue;
    map = merge_asset(map, assets['clean_'+rooms[i]]);
  }
  if (time > 11){
    map = merge_asset(map, assets['clock_'+(time-12)]);
  } else {
    map = merge_asset(map, assets['clock_'+time]);
  }
  for (var i = 0; i < characters.length; i++){
    if (positions[i] == null) continue;
    map = merge_asset(map, assets[characters[i]+'_'+rooms[positions[i]]]);
  }
  for (var i = 0; i < map.length; i++){
    to_print.push(map[i]+'\n');
  }
}

function print_room(room_id){
  var room = assets[rooms[room_id]];
  if (filth[room_id]) {
    for (var i = 0; i < filth[room_id]; i++ ){
      room = merge_asset(room, assets["filth_"+i]);
    }
  }
  for (var i = 0; i < room.length; i++){
    to_print.push(room[i]+'\n');
  }
}

function go_to(char_id, room_id) {
}

init();
print();
intro_menu(0);
